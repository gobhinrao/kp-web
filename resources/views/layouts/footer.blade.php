<!-- START FOOTER SECTION -->
<div class="footer-sec" id="footer-sec">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <div class="social-icons d-flex">
                    <a href="javascript:void(0)" class="wow fadeInUp"><i class="fab fa-facebook-f fb-icon"></i> </a>
                    <a href="javascript:void(0)" class="wow fadeInDown"><i class="fab fa-twitter twit-icon"></i> </a>
                    <a href="javascript:void(0)" class="wow fadeInUp"><i class="fab fa-google g-icon"></i> </a>
                    <a href="javascript:void(0)" class="wow fadeInDown"><i class="fab fa-linkedin-in link-icon"></i> </a>
                    <a href="javascript:void(0)" class="wow fadeInUp"><i class="fab fa-instagram insta-icon"></i> </a>
                    <a href="javascript:void(0)" class="wow fadeInDown"><i class="fas fa-envelope gmail-icon"></i> </a>
                </div>
            </div>

            <div class="col-12">
                <p class="rites text-center small-text">&copy; 2020 Megaone. Made with Love by Theme Industry</p>
            </div>
        </div>
    </div>
</div>
<!-- END FOOTER SECTION -->


<!-- JavaScript -->
<script src="{{ asset('vendor') }}/js/bundle.min.js"></script>
<!-- Plugin Js -->
<script src="{{ asset('vendor') }}/js/jquery.fancybox.min.js"></script>
<script src="{{ asset('vendor') }}/js/owl.carousel.min.js"></script>
<script src="{{ asset('vendor') }}/js/swiper.min.js"></script>
<script src="{{ asset('vendor') }}/js/jquery.cubeportfolio.min.js"></script>
<script src="{{ asset('vendor') }}/js/wow.min.js"></script>
<script src="{{ asset('vendor') }}/js/bootstrap-input-spinner.js"></script>
<script src="{{ asset('vendor') }}/js/parallaxie.min.js"></script>
<script src="{{ asset('vendor') }}/js/jquery.appear.js"></script>
<script src="{{ asset('vendor') }}/js/jquery.themepunch.tools.min.js"></script>
<script src="{{ asset('vendor') }}/js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION EXTENSIONS -->
<script src="{{ asset('vendor') }}/js/extensions/revolution.extension.actions.min.js"></script>
<script src="{{ asset('vendor') }}/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="{{ asset('vendor') }}/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="{{ asset('vendor') }}/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="{{ asset('vendor') }}/js/extensions/revolution.extension.migration.min.js"></script>
<script src="{{ asset('vendor') }}/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="{{ asset('vendor') }}/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="{{ asset('vendor') }}/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="{{ asset('vendor') }}/js/extensions/revolution.extension.video.min.js"></script>
<!-- FUNCTION Script -->
<script src="{{ asset('vendor') }}/js/contact_us.js"></script>
<script src="{{ asset('page') }}/js/script.js"></script>

</body>
</html>